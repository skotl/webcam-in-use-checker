using System;
using FakeItEasy;
using Xunit;

namespace Kwolo.WebCamInUseChecker.Tests
{
    public class RegistryValueToDateTimeTests
    {
        [Fact]
        public void NullRegistryKeyThrows()
        {
            Assert.Throws<ArgumentNullException>(() => new RegistryValueToDateTime(null, "", ""));
        }
        
        [Fact]
        public void ZeroValueReturnsZeroResult()
        {
            var regKey = A.Fake<IRegistryKey>();
            A.CallTo(() => regKey.GetValue("key", "value", long.MinValue))
                .WithAnyArguments()
                .Returns(0L);

            var converter = new RegistryValueToDateTime(regKey, "Key", "ValueName");

            Assert.Equal(RegistryValueToDateTime.CheckResponse.IsZero, converter.Check());
        }

        [Fact]
        public void MissingValueReturnsNotFoundResult()
        {
            var regKey = A.Fake<IRegistryKey>();
            A.CallTo(() => regKey.GetValue("key", "value", long.MinValue))
                .WithAnyArguments()
                .Returns(null);

            var converter = new RegistryValueToDateTime(regKey, "Key", "ValueName");

            Assert.Equal(RegistryValueToDateTime.CheckResponse.NotFound, converter.Check());
        }

        [Fact]
        public void FileTimeValueReturnsIsFoundResult()
        {
            var regKey = A.Fake<IRegistryKey>();
            A.CallTo(() => regKey.GetValue("key", "value", long.MinValue))
                .WithAnyArguments()
                .Returns(132015260350000000);

            var converter = new RegistryValueToDateTime(regKey, "Key", "ValueName");

            Assert.Equal(RegistryValueToDateTime.CheckResponse.IsFound, converter.Check());
        }

        [Fact]
        public void FileTimeValueSetsDateValue()
        {
            const long time = 132015260350000000;
            var timeAsDateTime = DateTime.FromFileTimeUtc(time);
            
            var regKey = A.Fake<IRegistryKey>();
            A.CallTo(() => regKey.GetValue("key", "value", long.MinValue))
                .WithAnyArguments()
                .Returns(time);

            var converter = new RegistryValueToDateTime(regKey, "Key", "ValueName");

            converter.Check();
            Assert.Equal(timeAsDateTime, converter.DateValue);
        }

        [Fact]
        public void NonNumericFileTimeValueThrows()
        {
            var regKey = A.Fake<IRegistryKey>();
            A.CallTo(() => regKey.GetValue("key", "value", long.MinValue))
                .WithAnyArguments()
                .Returns("rubbish");

            var converter = new RegistryValueToDateTime(regKey, "Key", "ValueName");

            Assert.Throws<InvalidCastException>(() => converter.Check());
        }
    }
}