﻿using System;
using FakeItEasy;
using Xunit;

namespace Kwolo.WebCamInUseChecker.Tests
{
    public static class WebCamAppValueSetterTests
    {
        [Fact]
        public static void ConstructorNullRegistryKeyThrows()
        {
            Assert.Throws<ArgumentNullException>(() => new WebCamAppValueSetter(null));
        }

        [Fact]
        public static void NullKeyThrows()
        {
            var regKey = A.Fake<IRegistryKey>();
            var setter = new WebCamAppValueSetter(regKey);

            Assert.Throws<ArgumentException>(() => setter.GetWebCamApp(null));
        }
        
        [Fact]
        public static void EmptyKeyThrows()
        {
            var regKey = A.Fake<IRegistryKey>();
            var setter = new WebCamAppValueSetter(regKey);

            Assert.Throws<ArgumentException>(() => setter.GetWebCamApp("  "));
        }

        [Fact]
        public static void MissingValuesSetsDatesNull()
        {
            var regKey = A.Fake<IRegistryKey>();
            A.CallTo(() => regKey.GetValue("key", "LastUsedTimeStart", long.MinValue))
                .Returns(null);
            A.CallTo(() => regKey.GetValue("key", "LastUsedTimeStop", long.MinValue))
                .Returns(null);
            
            var setter = new WebCamAppValueSetter(regKey);

            var app = setter.GetWebCamApp("key");
            Assert.Null(app.LastUsedTimeStart);
            Assert.Null(app.LastUsedTimeStop);
        }
        
        [Fact]
        public static void MissingValuesSetsInUseFalse()
        {
            var regKey = A.Fake<IRegistryKey>();
            A.CallTo(() => regKey.GetValue("key", "LastUsedTimeStart", long.MinValue))
                .Returns(null);
            A.CallTo(() => regKey.GetValue("key", "LastUsedTimeStop", long.MinValue))
                .Returns(null);
            
            var setter = new WebCamAppValueSetter(regKey);

            var app = setter.GetWebCamApp("key");
            Assert.False(app.IsInUse);
        }
        
        [Fact]
        public static void SetsWindowsExeValues()
        {
            const string key = "C:#programs#webcams#app.exe";
            
            var regKey = A.Fake<IRegistryKey>();
            A.CallTo(() => regKey.GetValue(key, "LastUsedTimeStart", long.MinValue))
                .Returns(null);
            A.CallTo(() => regKey.GetValue(key, "LastUsedTimeStop", long.MinValue))
                .Returns(null);
            
            var setter = new WebCamAppValueSetter(regKey);
            var app = setter.GetWebCamApp(key);
            
            Assert.Equal(@"C:\programs\webcams\app.exe", app.AppAsFilePath);
            Assert.Equal(@"app.exe", app.AppAsFileName);
            Assert.Equal(key, app.AppName);
        }
        
        [Fact]
        public static void SetsStoreExeValues()
        {
            const string key = "Microsoft.WebCamApp";
            
            var regKey = A.Fake<IRegistryKey>();
            A.CallTo(() => regKey.GetValue(key, "LastUsedTimeStart", long.MinValue))
                .Returns(null);
            A.CallTo(() => regKey.GetValue(key, "LastUsedTimeStop", long.MinValue))
                .Returns(null);
            
            var setter = new WebCamAppValueSetter(regKey);
            var app = setter.GetWebCamApp(key);
            
            Assert.Equal(key, app.AppAsFilePath);
            Assert.Equal(key, app.AppAsFileName);
            Assert.Equal(key, app.AppName);
        }
        
        [Fact]
        public static void SetsLastStartTime()
        {
            const string key = "Microsoft.WebCamApp";
            const long time = 132015260350000000;
            var timeAsDateTime = DateTime.FromFileTimeUtc(time);
            
            var regKey = A.Fake<IRegistryKey>();
            A.CallTo(() => regKey.GetValue(key, "LastUsedTimeStart", long.MinValue))
                .Returns(time);
            A.CallTo(() => regKey.GetValue(key, "LastUsedTimeStop", long.MinValue))
                .Returns(null);
            
            var setter = new WebCamAppValueSetter(regKey);
            var app = setter.GetWebCamApp(key);
            
            Assert.Equal(timeAsDateTime, app.LastUsedTimeStart);
        }
        
        [Fact]
        public static void SetsLastStopTime()
        {
            const string key = "Microsoft.WebCamApp";
            const long time = 132015260350000000;
            var timeAsDateTime = DateTime.FromFileTimeUtc(time);
            
            var regKey = A.Fake<IRegistryKey>();
            A.CallTo(() => regKey.GetValue(key, "LastUsedTimeStart", long.MinValue))
                .Returns(null);
            A.CallTo(() => regKey.GetValue(key, "LastUsedTimeStop", long.MinValue))
                .Returns(time);
            
            var setter = new WebCamAppValueSetter(regKey);
            var app = setter.GetWebCamApp(key);
            
            Assert.Equal(timeAsDateTime, app.LastUsedTimeStop);
        }
        
        [Fact]
        public static void StartAndStopTimeMeansNotInUser()
        {
            const string key = "Microsoft.WebCamApp";
            const long time = 132015260350000000;
            var timeAsDateTime = DateTime.FromFileTimeUtc(time);
            
            var regKey = A.Fake<IRegistryKey>();
            A.CallTo(() => regKey.GetValue(key, "LastUsedTimeStart", long.MinValue))
                .Returns(time);
            A.CallTo(() => regKey.GetValue(key, "LastUsedTimeStop", long.MinValue))
                .Returns(time);
            
            var setter = new WebCamAppValueSetter(regKey);
            var app = setter.GetWebCamApp(key);
            
            Assert.False(app.IsInUse);
        }
        
        [Fact]
        public static void StartTimeWithZeroStopMeansInUse()
        {
            const string key = "Microsoft.WebCamApp";
            const long time = 132015260350000000;
            var timeAsDateTime = DateTime.FromFileTimeUtc(time);
            
            var regKey = A.Fake<IRegistryKey>();
            A.CallTo(() => regKey.GetValue(key, "LastUsedTimeStart", long.MinValue))
                .Returns(time);
            A.CallTo(() => regKey.GetValue(key, "LastUsedTimeStop", long.MinValue))
                .Returns(0L);
            
            var setter = new WebCamAppValueSetter(regKey);
            var app = setter.GetWebCamApp(key);
            
            Assert.True(app.IsInUse);
        }
    }
}