﻿using System;
using System.Runtime.Versioning;
using Microsoft.Win32;

namespace Kwolo.WebCamInUseChecker
{
    /// <summary>
    /// Concrete implementation of <see cref="IRegistryKey"/> that calls into the Win32 Registry
    /// </summary>
    [SupportedOSPlatform("windows")]
    public class Win32RegistryKeyWrapper : IRegistryKey
    {
        private readonly RegistryKey? _registryKey;
        
        public Win32RegistryKeyWrapper(RegistryKey? registryKey)
        {
            _registryKey = registryKey;
        }

        public string? Name => _registryKey?.ToString();

        public IRegistryKey? OpenSubKey(string key)
        {
            return new Win32RegistryKeyWrapper(_registryKey?.OpenSubKey(key));
        }

        public string[] GetSubKeyNames()
        {
            return _registryKey?.GetSubKeyNames() ?? throw new ArgumentNullException(nameof(_registryKey));
        }

        public object? GetValue(string key, string valueName, in object defaultValue)
        {
            return Registry.GetValue(key, valueName, defaultValue);
        }

        public void Dispose()
        {
            _registryKey?.Dispose();
        }
        
    }
}