﻿using System;

namespace Kwolo.WebCamInUseChecker
{
    public interface IRegistryKey : IDisposable
    {
        string? Name { get; }
        IRegistryKey? OpenSubKey(string key);
        string[] GetSubKeyNames();
        object? GetValue(string key, string valueName, in object defaultValue);
    }
}