﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Versioning;
using Kwolo.WebCamInUseChecker.Models;

namespace Kwolo.WebCamInUseChecker
{
    /// <summary>
    /// An application that is registered to use a webcam
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal class WebCamAppValueSetter
    {
        private readonly IRegistryKey _registryKey;
        private const string LastUsedTimeStartName = "LastUsedTimeStart";
        private const string LastUsedTimeStopName = "LastUsedTimeStop";

        public WebCamAppValueSetter(IRegistryKey registryKey)
        {
            _registryKey = registryKey ?? throw new ArgumentNullException(nameof(registryKey));
        }

        /// <summary>
        /// Check the application's configuration and populate the last start and stop times 
        /// </summary>
        public WebCamApp GetWebCamApp(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
                throw new ArgumentException(null, nameof(key));
            
            var app = new WebCamApp();
            DefaultValues(app, key);

            SetStartTime(app, key);
            SetStopTime(app, key);

            return app;
        }

        /// <summary>
        /// Read the registry value for the last start time, and set the start property if found
        /// </summary>
        /// <param name="webCamApp"></param>
        /// <param name="key"></param>
        private void SetStartTime(WebCamApp webCamApp, string key)
        {
            var lastUsedStart = new RegistryValueToDateTime(_registryKey, key, LastUsedTimeStartName);

            if (lastUsedStart.Check() == RegistryValueToDateTime.CheckResponse.IsFound)
                webCamApp.LastUsedTimeStart = lastUsedStart.DateValue;
        }

        /// <summary>
        /// Read the registry value for the last stop time, and set the start property if found
        /// </summary>
        /// <param name="webCamApp"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private void SetStopTime(WebCamApp webCamApp, string key)
        {
            var lastUsedStop = new RegistryValueToDateTime(_registryKey, key, LastUsedTimeStopName);

            if (lastUsedStop.Check() == RegistryValueToDateTime.CheckResponse.IsFound)
                webCamApp.LastUsedTimeStop = lastUsedStop.DateValue;
        }

        /// <summary>
        /// Set default values
        /// </summary>
        /// <param name="webCamApp"></param>
        /// <param name="key"></param>
        private static void DefaultValues(WebCamApp webCamApp, string key)
        {
            webCamApp.LastUsedTimeStart = null;
            webCamApp.LastUsedTimeStop = null;
            
            webCamApp.AppName = key?.Split('\\').ToList().LastOrDefault();
            webCamApp.AppAsFilePath = webCamApp.AppName?.Replace('#', '\\');
            webCamApp.AppAsFileName = Path.GetFileName(webCamApp.AppAsFilePath ?? "none");
        }

    }
}