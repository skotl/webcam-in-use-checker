﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using Kwolo.WebCamInUseChecker.Models;
using Microsoft.Win32;

namespace Kwolo.WebCamInUseChecker
{
    /// <summary>
    /// Enumerates registry keys to discover registered webcam apps
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal class WebCamRegistryKeyEnumerator
    {
        private readonly IRegistryKey _registryKey;
        private const string WebCamKeyPath =
            @"SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\webcam";

        public WebCamRegistryKeyEnumerator(IRegistryKey registryKey)
        {
            _registryKey = registryKey;
        }

        public WebCamRegistryKeyEnumerator()
        {
            _registryKey = new Win32RegistryKeyWrapper(Registry.CurrentUser);
        }
        
        /// <summary>
        /// Return a list of all web cam registry keys under the base path
        /// </summary>
        /// <returns></returns>
        public IEnumerable<WebCamRegistryKey> GetSubKeys()
        {
            var list = new List<WebCamRegistryKey>();
            var toProcess = new Stack<string>();
            toProcess.Push(WebCamKeyPath);

            while (toProcess.Count > 0)
            {
                GetSubKeys(toProcess.Pop()).ForEach(sk =>
                {
                    var regKey = new WebCamRegistryKey(_registryKey, sk);
                    list.Add(regKey);
                    toProcess.Push(sk);
                });
            }

            return list;
        }

        private List<string> GetSubKeys(string key)
        {
            using var regKey = _registryKey.OpenSubKey(key);
            return regKey == null ? new List<string>() : regKey.GetSubKeyNames()
                .Select(k => string.Concat(key, "\\", k))
                .ToList();
        }
    }
}