﻿using System;
using System.Runtime.Versioning;

namespace Kwolo.WebCamInUseChecker
{
    /// <summary>
    /// Reads a registry value in FileTime format (64bit integer) and sets a <see cref="DateTime"/> value if found
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal class RegistryValueToDateTime
    {
        private const long DefaultValue = long.MinValue;
        private readonly IRegistryKey _registryKey;
        private readonly string _key;
        private readonly string _valueName;

        /// <summary>
        /// Type of response returned
        /// </summary>
        public enum CheckResponse
        {
            NotFound,
            IsZero,
            IsFound
        }

        /// <summary>
        /// If <see cref="CheckResponse"/> is found then this is set to the <see cref="DateTime"/> representation
        /// of the value, null if otherwise
        /// </summary>
        public DateTime DateValue { get; private set; }
        
        public RegistryValueToDateTime(IRegistryKey registryKey, string key, string valueName)
        {
            _registryKey = registryKey ?? throw new ArgumentNullException(nameof(registryKey));
            _key = key;
            _valueName = valueName;
        }

        /// <summary>
        /// Checks whether the value exists, and is zero or set to a date value
        /// </summary>
        /// <returns></returns>
        public CheckResponse Check()
        {
            var value = (long) (_registryKey.GetValue(_key, _valueName, DefaultValue) ?? DefaultValue);
            switch (value)
            {
                case DefaultValue:
                    return CheckResponse.NotFound;
                case 0:
                    return CheckResponse.IsZero;
                default:
                    DateValue = DateTime.FromFileTimeUtc(value);
                    return CheckResponse.IsFound;
            }
        }
    }
}