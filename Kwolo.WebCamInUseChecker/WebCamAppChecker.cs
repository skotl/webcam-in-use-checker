﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using Kwolo.WebCamInUseChecker.Models;

namespace Kwolo.WebCamInUseChecker
{
    [SupportedOSPlatform("windows")]
    public class WebCamAppChecker
    {
        private readonly IRegistryKey _registryKey;

        public WebCamAppChecker(IRegistryKey registryKey)
        {
            _registryKey = registryKey;
        }
        
        /// <summary>
        /// Return the application that is currently using a webcam, or null if none are
        /// </summary>
        /// <returns></returns>
        public WebCamApp? FindAppUsingCamera()
        {
            return GetAllWebCamApps().FirstOrDefault(a => a.IsInUse);
        }

        /// <summary>
        /// Return the list of applications that have consent to use a webcam
        /// </summary>
        /// <returns></returns>
        public List<WebCamApp> GetAllWebCamApps()
        {
            var allKeys = new WebCamRegistryKeyEnumerator().GetSubKeys();
            var allApps = new List<WebCamApp>();

            foreach (var key in allKeys)
            {
                var webcamChecker = new WebCamAppValueSetter(_registryKey);
                allApps.Add(webcamChecker.GetWebCamApp(key.FullPath));
            }

            return allApps;
        }
    }
}