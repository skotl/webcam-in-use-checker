﻿using System.Runtime.Versioning;

namespace Kwolo.WebCamInUseChecker.Models
{
    /// <summary>
    /// A registry key that describes a webcam
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal class WebCamRegistryKey
    {
        /// <summary>
        /// The registry that the key us in
        /// </summary>
        public IRegistryKey Registry { get; set; }
        
        /// <summary>
        /// The key to the webcam app description
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// The full path, including hive, of the app's registry key
        /// </summary>
        public string FullPath => string.Concat(Registry?.Name, "\\", Key);

        public WebCamRegistryKey(IRegistryKey registry, string key)
        {
            Registry = registry;
            Key = key;
        }

        public override string ToString()
        {
            return FullPath;
        }
    }
}