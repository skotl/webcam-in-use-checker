﻿using System;
using System.IO;
using System.Linq;

namespace Kwolo.WebCamInUseChecker.Models
{
    public class WebCamApp
    {
        /// <summary>
        /// True if the app is currently using a webcam
        /// </summary>
        public bool IsInUse => LastUsedTimeStart != null && LastUsedTimeStop == null;

        /// <summary>
        /// The time that the app last started using a webcam
        /// </summary>
        public DateTime? LastUsedTimeStart { get; set; }

        /// <summary>
        /// The time that the app last stopped using a webcam
        /// </summary>
        public DateTime? LastUsedTimeStop { get;  set; }

        /// <summary>
        /// The name of the application, decoded from the registry key
        /// </summary>
        public string? AppName { get; set; }

        /// <summary>
        /// For non-Windows Store Apps, the file path to the app's executable
        /// </summary>
        public string? AppAsFilePath { get; set; }

        /// <summary>
        /// For non-Windows Store Apps, the name of the executable
        /// </summary>
        public string? AppAsFileName { get; set; }
        
        public override string ToString()
        {
            var inUse = IsInUse ? " [Using webcam]" : "";
            return $"{AppAsFileName}{inUse}, {LastUsedTimeStart?.ToLocalTime()}-{LastUsedTimeStop?.ToLocalTime()}";
        }
    }
}