﻿using System;
using Microsoft.Win32;
using Kwolo.WebCamInUseChecker;

if (OperatingSystem.IsWindows())
{
    var checker = new WebCamAppChecker(new Win32RegistryKeyWrapper(Registry.CurrentUser));
    var usingApp = checker.FindAppUsingCamera();
    var inUseString = usingApp != null ? "in use by " : "not in use";

    Console.WriteLine($"Found out that webcam is {inUseString}{usingApp}");
    Console.WriteLine();

    Console.WriteLine("All apps registered for a webcam:");

    foreach (var webCamApp in checker.GetAllWebCamApps())
    {
        Console.WriteLine($"   {webCamApp}");
    }
}
else
{
    Console.WriteLine("This application is only supported on Windows");
}
