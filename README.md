# WebCam in-use checker

I searched in vain for a solution for checking whether an app was using the webcam, looking at all sorts of video and USB enumeration options and came up blank.

Then I stumbled on the "consent" options in Windows 10 version 20H1 and on, and discovered that Windows did some magic where it recorded what time an app starts and stops using a webcam. 
This then allowed me to do some basic registry checks to see whether an app was using the camera, by looking for an app's registry key that has a valid *start* date/time but a zero *stop* date/time.

# Solution makeup
There's a class library, `WebCamInUseChecker`, and a sample app `SampleApp`.

The projects are all .Net 5 (aka .Net Core 5) but I've also tested them on .Net Core 3 so there shouldn't be any problem getting them running on earlier versions (see below).

# Using the library
Check the sample app, but there's only one public class that you need to instantiate and then there are two methods on that:

```csharp
//Create a new checker
var checker = new WebCamAppChecker();

// Find out whether an app is in use
var usingApp = checker.FindAppUsingCamera();
var inUseString = usingApp != null ? "in use by " : "not in use";

Console.WriteLine($"Found out that webcam is {inUseString}{usingApp}");

// Get all the apps currently registered to use a webcam
foreach (var webCamApp in checker.GetAllWebCamApps())
{
    Console.WriteLine($"   {webCamApp}");
}
```


Both methods return a `WebCamApp` object / list, and the most interesting properties on this are:

 * `AppName` the name of the application
 * `AppAsFilePath` for non Windows-Store apps, the path to the executable
 * `AppAsFileName` for non Windows-Store apps, the filename of the executable
 * `LastUsedTimeStart` the time the app last started using the webcam (or null, if never)
 * `LastUsedTimeStop` the time the app last stopped using the webcam
 * `IsInUse` true if the app is currently use a webcam


# Building in an earlier version of .Net Core
If you aren't ready to use this in .Net 5 then you can use an earlier version of .Net Core. The changes you need to make are:

1. Remove all references to `[SupportedOSPlatform("windows")]` in the Class Library project files
1. Rewrite the sample app (if you need it) into a Main() type program - just copy the existing code into your Main()
1. Remove the `if (OperatingSystem.IsWindows())` construct from Program.cs

# Nuget package
Package is available at https://www.nuget.org/packages/kwolo.WebCamInUseChecker

# Contributions
Knock yourself out! Happy to see contributions.
